# HTML
## What You Need
If you want to develop Web pages with *HTML*, then we suggest
the following programs and tools

- [Node.js](https://nodejs.org/en), your runtime environment
- [npm](https://www.npmjs.com/), a package manager
- [Visual Sudio Code](https://code.visualstudio.com/), your IDE or some other,equivalent tool or editor
- [GIT](https://git-scm.com/), your version control system

# Demos and Exercises
The demos and exercises are organized in *GIT* branches. The demo branches start with *demo/...* and the exercise branches start with *exercise/...*

## The Trail
The trail is a good way to learn things step by step. The demos of the trail can be found under *demo/trail/...* and the exercises under *exercise/trail/...*

This is the sequence, in which the trail is organized

1. *demo/trail/intro*
