# Exercise
The `TodoListComponent` iterates over all available `Todos` and displays a `TodoListItemComponent` for each `Todo`. Currently each `TodoListItemComponent` displays the same `Todo`.

## The Input Decorator
The `TodoListItemComponent` components are child components of the `TodoListComponent`. If we want to pass the single `Todo` object to the child components, then we need the `@Input` decorator.

1. Replace the hard-coded assignment of a `Todo` instance in the `TodoListItemComponent` by a variable that can be assigned from outside.

1. Now pass the `Todos` from the template of the `TodoListComponent` to the `TodoListItemComponent` child components.

# Solution
## The Input Decorator
When we add the `@Input` decorator to the `TodoListItemComponent`, then the code looks as follows

```
@Component({ ... })
export class TodoListItemComponent {
  @Input() todo!: Todo;
}
```

In the template of the parent component we pass the `Todos` to the `todo` property of the child components

```
<ul>
    <li *ngFor="let item of todos; let i = index;">
        {{ i+1 }}. <app-todo-list-item [todo]="item"></app-todo-list-item>
    </li>
</ul>
```

> **Note** We changed the name of the loop variable from `todo` to `item`. This makes is easier to distinguish between the parent property `item` and the bound child property `todo`. We only did it for better readability! Of course we could have left the loop variable with its previous name.

