# What You Will Learn About
In this section you will learn about

- event binding

# Event Bindings
## Event Binding in HTML
### Inlined JavaScript Expressions
We can listen to events emitted by *HTML* elements by adding a *JavaScript* expression to the desired event attribute.

```
<div onclick="alert('Hello')">Say Hello</div>
```

The `onclick` attribute identifies the *click* event. When the `div` element is clicked, then the specified *JavaScript* expression is executed. Here the built-in `alert(...)` function is run, which opens a small dialog, displaying *Hello*.

### Custom Functions
Of course we can also use custom *JavaScript* functions.

**JavaScript**

```
function sayHello() {
    console.log('Hello');
}
```

**HTML**

```
<div onclick="sayHello()">Say Hello</div>
```

### Passing the Event Object
An event is an object fired by some *HTML* element. If we want to access the content of the event object, we can add an event parameter to the handler function.

Our *JavaScript* function then looks like this

**JavaScript**

```
function sayHello(evt) {
    console.log('Hello from ' + evt.target);
}
```

In *HTML* the event object has the name 'event'.

**HTML**

```
<div onclick="sayHello(event)">Say Hello</div>
```

## What is an Event Binding?
Event handling in *Angular* is very similar to the way it is done in *HTML*. Instead of using the *HTML* attribute `onclick` the simple event name is used, in this case `click`.

The intention of *Angular* is not to call some arbitrary *JavaScript* function, but a method of the component class. That's why talk about *event binding*. A method of the component class is bound to some *HTML* event. Syntactically this is expressed by round brackets `()` around the event name, e.g. `(click)`.

**Component**

```
@Component({...})
export class MyComponent {
    sayHello() {
        console.log('Hello');
    }
}
```

**HTML Template**

```
<div (click)="sayHello()">Say Hello</div>
```

